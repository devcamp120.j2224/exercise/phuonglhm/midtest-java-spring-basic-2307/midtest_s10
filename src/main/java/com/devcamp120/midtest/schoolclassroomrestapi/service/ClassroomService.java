package com.devcamp120.midtest.schoolclassroomrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp120.midtest.schoolclassroomrestapi.model.Classroom;

@Service
public class ClassroomService {
    Classroom class11 = new Classroom(11,"1A",40);
    Classroom class12 = new Classroom(12,"2A",45);
    Classroom class13 = new Classroom(13,"3A",50);

    Classroom class21 = new Classroom(21, "1B", 30);
    Classroom class22 = new Classroom(22, "2B", 35);
    Classroom class23 = new Classroom(23, "3B", 40);

    Classroom class31 = new Classroom(31, "1C", 50);
    Classroom class32 = new Classroom(32, "2C", 45);
    Classroom class33 = new Classroom(33, "3C", 40);

    public ArrayList<Classroom> getClassSchool1(){
        ArrayList<Classroom> ClassSchool1 = new ArrayList<Classroom>();

        ClassSchool1.add(class11);
        ClassSchool1.add(class12);
        ClassSchool1.add(class13);

        return ClassSchool1;
    }

    public ArrayList<Classroom> getClassSchool2(){
        ArrayList<Classroom> ClassSchool2 = new ArrayList<Classroom>();

        ClassSchool2.add(class21);
        ClassSchool2.add(class22);
        ClassSchool2.add(class23);

        return ClassSchool2;
    }

    public ArrayList<Classroom> getClassSchool3(){
        ArrayList<Classroom> ClassSchool3 = new ArrayList<Classroom>();

        ClassSchool3.add(class31);
        ClassSchool3.add(class32);
        ClassSchool3.add(class33);

        return ClassSchool3;
    }

    public ArrayList<Classroom> getAllClass(){
        ArrayList<Classroom> classes = new ArrayList<Classroom>();
            classes.add(class11);
            classes.add(class12);
            classes.add(class13);
            classes.add(class21);
            classes.add(class22);
            classes.add(class23);
            classes.add(class31);
            classes.add(class32);
            classes.add(class33);
        return classes;
    }

}
