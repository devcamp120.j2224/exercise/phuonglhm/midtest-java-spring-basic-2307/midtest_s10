package com.devcamp120.midtest.schoolclassroomrestapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp120.midtest.schoolclassroomrestapi.model.School;

@Service
public class SchoolService {
    @Autowired
    private ClassroomService classroomService;

    public ArrayList<School> getAllSchool(){

    School school1 = new School(111, "A", "school1Address",classroomService.getClassSchool1());
    School school2 = new School(222, "B", "school1Address",classroomService.getClassSchool2());
    School school3 = new School(333, "C", "school1Address",classroomService.getClassSchool3());

        ArrayList<School> listSchool = new ArrayList<School>();

        listSchool.add(school1);
        listSchool.add(school2);
        listSchool.add(school3);

        return listSchool;
    }

}
