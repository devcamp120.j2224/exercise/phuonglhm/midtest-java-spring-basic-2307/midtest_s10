package com.devcamp120.midtest.schoolclassroomrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp120.midtest.schoolclassroomrestapi.model.School;
import com.devcamp120.midtest.schoolclassroomrestapi.service.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)

public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping("/schools") //lấy toàn bộ danh sách trường
    public ArrayList<School> getAllSchool() {
        ArrayList<School> allSchool = schoolService.getAllSchool();

        return allSchool;
    }

    @GetMapping("/school-info")//School tương ứng schoolId
        public School getSchoolInfo(@RequestParam(required = true, name="SchoolId") int SchoolId) {
            ArrayList<School> allSchool = schoolService.getAllSchool();

            School findSchool = new School();

            for (School school : allSchool) {
                if(school.getId() == SchoolId) {
                    findSchool = school;
                }
            }
            return findSchool;
        }

        @GetMapping("/SchoolMoreNoSudent") //trả ra danh sách trường có số học sinh lớn hơn noNumber
        public ArrayList<School> getSchoolMoreNoStudent(@RequestParam(required = true, name="noNumber") int noNumber){
            ArrayList<School> allSchool = schoolService.getAllSchool();

            ArrayList<School> BiggerSchool = new ArrayList<School>();
            for(int i = 0; i < allSchool.size(); i++ ){
                if(allSchool.get(i).getTotalStudent() > noNumber) {
                    BiggerSchool.add(allSchool.get(i));
                }
            }
            return BiggerSchool;
        }
    }

    
