package com.devcamp120.midtest.schoolclassroomrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp120.midtest.schoolclassroomrestapi.model.Classroom;
import com.devcamp120.midtest.schoolclassroomrestapi.service.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ClassController {
    @Autowired
    private ClassroomService classroomService;
    @GetMapping("/classes")
        public ArrayList<Classroom> getAllClasses() {
            ArrayList<Classroom> allClasses = classroomService.getAllClass();
            return allClasses;
        }

    @GetMapping("/moreNoSudent")
        public ArrayList<Classroom> getClassMoreNoStudent(@RequestParam(required = true, name="noNumber") int noNumber){
            ArrayList<Classroom> allClasses = classroomService.getAllClass();

            ArrayList<Classroom> BiggerClass = new ArrayList<>();
            for(int i = 0; i < allClasses.size(); i++ ){
                if(allClasses.get(i).getNoStudent() > noNumber) {
                    BiggerClass.add(allClasses.get(i));
                }
            }
            return BiggerClass;
        }
}

