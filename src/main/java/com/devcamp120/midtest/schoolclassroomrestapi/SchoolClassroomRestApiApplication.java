package com.devcamp120.midtest.schoolclassroomrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolClassroomRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolClassroomRestApiApplication.class, args);
	}

}
